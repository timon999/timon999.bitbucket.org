
var Module;

if (typeof Module === 'undefined') Module = eval('(function() { try { return Module || {} } catch(e) { return {} } })()');

if (!Module.expectedDataFileDownloads) {
  Module.expectedDataFileDownloads = 0;
  Module.finishedDataFileDownloads = 0;
}
Module.expectedDataFileDownloads++;
(function() {
 var loadPackage = function(metadata) {

    var PACKAGE_PATH;
    if (typeof window === 'object') {
      PACKAGE_PATH = window['encodeURIComponent'](window.location.pathname.toString().substring(0, window.location.pathname.toString().lastIndexOf('/')) + '/');
    } else if (typeof location !== 'undefined') {
      // worker
      PACKAGE_PATH = encodeURIComponent(location.pathname.toString().substring(0, location.pathname.toString().lastIndexOf('/')) + '/');
    } else {
      throw 'using preloaded data can only be done on a web page or in a web worker';
    }
    var PACKAGE_NAME = 'game.data';
    var REMOTE_PACKAGE_BASE = 'game.data';
    if (typeof Module['locateFilePackage'] === 'function' && !Module['locateFile']) {
      Module['locateFile'] = Module['locateFilePackage'];
      Module.printErr('warning: you defined Module.locateFilePackage, that has been renamed to Module.locateFile (using your locateFilePackage for now)');
    }
    var REMOTE_PACKAGE_NAME = typeof Module['locateFile'] === 'function' ?
                              Module['locateFile'](REMOTE_PACKAGE_BASE) :
                              ((Module['filePackagePrefixURL'] || '') + REMOTE_PACKAGE_BASE);
  
    var REMOTE_PACKAGE_SIZE = metadata.remote_package_size;
    var PACKAGE_UUID = metadata.package_uuid;
  
    function fetchRemotePackage(packageName, packageSize, callback, errback) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', packageName, true);
      xhr.responseType = 'arraybuffer';
      xhr.onprogress = function(event) {
        var url = packageName;
        var size = packageSize;
        if (event.total) size = event.total;
        if (event.loaded) {
          if (!xhr.addedTotal) {
            xhr.addedTotal = true;
            if (!Module.dataFileDownloads) Module.dataFileDownloads = {};
            Module.dataFileDownloads[url] = {
              loaded: event.loaded,
              total: size
            };
          } else {
            Module.dataFileDownloads[url].loaded = event.loaded;
          }
          var total = 0;
          var loaded = 0;
          var num = 0;
          for (var download in Module.dataFileDownloads) {
          var data = Module.dataFileDownloads[download];
            total += data.total;
            loaded += data.loaded;
            num++;
          }
          total = Math.ceil(total * Module.expectedDataFileDownloads/num);
          if (Module['setStatus']) Module['setStatus']('Downloading data... (' + loaded + '/' + total + ')');
        } else if (!Module.dataFileDownloads) {
          if (Module['setStatus']) Module['setStatus']('Downloading data...');
        }
      };
      xhr.onload = function(event) {
        var packageData = xhr.response;
        callback(packageData);
      };
      xhr.send(null);
    };

    function handleError(error) {
      console.error('package error:', error);
    };
  
      var fetched = null, fetchedCallback = null;
      fetchRemotePackage(REMOTE_PACKAGE_NAME, REMOTE_PACKAGE_SIZE, function(data) {
        if (fetchedCallback) {
          fetchedCallback(data);
          fetchedCallback = null;
        } else {
          fetched = data;
        }
      }, handleError);
    
  function runWithFS() {

    function assert(check, msg) {
      if (!check) throw msg + new Error().stack;
    }
Module['FS_createPath']('/', 'lib', true, true);
Module['FS_createPath']('/lib', 'hump', true, true);
Module['FS_createPath']('/', '.git', true, true);
Module['FS_createPath']('/.git', 'logs', true, true);
Module['FS_createPath']('/.git/logs', 'refs', true, true);
Module['FS_createPath']('/.git/logs/refs', 'heads', true, true);
Module['FS_createPath']('/.git/logs/refs', 'remotes', true, true);
Module['FS_createPath']('/.git/logs/refs/remotes', 'origin', true, true);
Module['FS_createPath']('/.git', 'info', true, true);
Module['FS_createPath']('/.git', 'hooks', true, true);
Module['FS_createPath']('/.git', 'objects', true, true);
Module['FS_createPath']('/.git/objects', 'pack', true, true);
Module['FS_createPath']('/.git', 'refs', true, true);
Module['FS_createPath']('/.git/refs', 'heads', true, true);
Module['FS_createPath']('/.git/refs', 'remotes', true, true);
Module['FS_createPath']('/.git/refs/remotes', 'origin', true, true);
Module['FS_createPath']('/', 'assets', true, true);

    function DataRequest(start, end, crunched, audio) {
      this.start = start;
      this.end = end;
      this.crunched = crunched;
      this.audio = audio;
    }
    DataRequest.prototype = {
      requests: {},
      open: function(mode, name) {
        this.name = name;
        this.requests[name] = this;
        Module['addRunDependency']('fp ' + this.name);
      },
      send: function() {},
      onload: function() {
        var byteArray = this.byteArray.subarray(this.start, this.end);

          this.finish(byteArray);

      },
      finish: function(byteArray) {
        var that = this;

        Module['FS_createDataFile'](this.name, null, byteArray, true, true, true); // canOwn this data in the filesystem, it is a slide into the heap that will never change
        Module['removeRunDependency']('fp ' + that.name);

        this.requests[this.name] = null;
      },
    };

        var files = metadata.files;
        for (i = 0; i < files.length; ++i) {
          new DataRequest(files[i].start, files[i].end, files[i].crunched, files[i].audio).open('GET', files[i].filename);
        }

  
    function processPackageData(arrayBuffer) {
      Module.finishedDataFileDownloads++;
      assert(arrayBuffer, 'Loading data file failed.');
      assert(arrayBuffer instanceof ArrayBuffer, 'bad input to processPackageData');
      var byteArray = new Uint8Array(arrayBuffer);
      var curr;
      
        // copy the entire loaded file into a spot in the heap. Files will refer to slices in that. They cannot be freed though
        // (we may be allocating before malloc is ready, during startup).
        if (Module['SPLIT_MEMORY']) Module.printErr('warning: you should run the file packager with --no-heap-copy when SPLIT_MEMORY is used, otherwise copying into the heap may fail due to the splitting');
        var ptr = Module['getMemory'](byteArray.length);
        Module['HEAPU8'].set(byteArray, ptr);
        DataRequest.prototype.byteArray = Module['HEAPU8'].subarray(ptr, ptr+byteArray.length);
  
          var files = metadata.files;
          for (i = 0; i < files.length; ++i) {
            DataRequest.prototype.requests[files[i].filename].onload();
          }
              Module['removeRunDependency']('datafile_game.data');

    };
    Module['addRunDependency']('datafile_game.data');
  
    if (!Module.preloadResults) Module.preloadResults = {};
  
      Module.preloadResults[PACKAGE_NAME] = {fromCache: false};
      if (fetched) {
        processPackageData(fetched);
        fetched = null;
      } else {
        fetchedCallback = processPackageData;
      }
    
  }
  if (Module['calledRun']) {
    runWithFS();
  } else {
    if (!Module['preRun']) Module['preRun'] = [];
    Module["preRun"].push(runWithFS); // FS is not initialized yet, wait for it
  }

 }
 loadPackage({"files": [{"audio": 0, "start": 0, "crunched": 0, "end": 4801, "filename": "/main.lua"}, {"audio": 0, "start": 4801, "crunched": 0, "end": 36805, "filename": "/LICENSE"}, {"audio": 0, "start": 36805, "crunched": 0, "end": 37778, "filename": "/unittests.lua"}, {"audio": 0, "start": 37778, "crunched": 0, "end": 38331, "filename": "/README.md"}, {"audio": 0, "start": 38331, "crunched": 0, "end": 39165, "filename": "/moontest.lua"}, {"audio": 0, "start": 39165, "crunched": 0, "end": 41054, "filename": "/levelgen.lua"}, {"audio": 0, "start": 41054, "crunched": 0, "end": 42827, "filename": "/levelsolved.lua"}, {"audio": 0, "start": 42827, "crunched": 0, "end": 42869, "filename": "/parameters.lua"}, {"audio": 0, "start": 42869, "crunched": 0, "end": 43006, "filename": "/conf.lua"}, {"audio": 0, "start": 43006, "crunched": 0, "end": 43082, "filename": "/build"}, {"audio": 0, "start": 43082, "crunched": 0, "end": 43890, "filename": "/lib/deepcompare.lua"}, {"audio": 0, "start": 43890, "crunched": 0, "end": 53075, "filename": "/lib/inspect.lua"}, {"audio": 0, "start": 53075, "crunched": 0, "end": 56466, "filename": "/lib/hump/gamestate.lua"}, {"audio": 0, "start": 56466, "crunched": 0, "end": 59235, "filename": "/lib/hump/signal.lua"}, {"audio": 0, "start": 59235, "crunched": 0, "end": 61434, "filename": "/lib/hump/README.md"}, {"audio": 0, "start": 61434, "crunched": 0, "end": 66757, "filename": "/lib/hump/vector.lua"}, {"audio": 0, "start": 66757, "crunched": 0, "end": 69781, "filename": "/lib/hump/class.lua"}, {"audio": 0, "start": 69781, "crunched": 0, "end": 75341, "filename": "/lib/hump/camera.lua"}, {"audio": 0, "start": 75341, "crunched": 0, "end": 81565, "filename": "/lib/hump/timer.lua"}, {"audio": 0, "start": 81565, "crunched": 0, "end": 85125, "filename": "/lib/hump/vector-light.lua"}, {"audio": 0, "start": 85125, "crunched": 0, "end": 85148, "filename": "/.git/HEAD"}, {"audio": 0, "start": 85148, "crunched": 0, "end": 87628, "filename": "/.git/index"}, {"audio": 0, "start": 87628, "crunched": 0, "end": 87701, "filename": "/.git/description"}, {"audio": 0, "start": 87701, "crunched": 0, "end": 87808, "filename": "/.git/packed-refs"}, {"audio": 0, "start": 87808, "crunched": 0, "end": 88076, "filename": "/.git/config"}, {"audio": 0, "start": 88076, "crunched": 0, "end": 88272, "filename": "/.git/logs/HEAD"}, {"audio": 0, "start": 88272, "crunched": 0, "end": 88468, "filename": "/.git/logs/refs/heads/master"}, {"audio": 0, "start": 88468, "crunched": 0, "end": 88664, "filename": "/.git/logs/refs/remotes/origin/HEAD"}, {"audio": 0, "start": 88664, "crunched": 0, "end": 88904, "filename": "/.git/info/exclude"}, {"audio": 0, "start": 88904, "crunched": 0, "end": 89800, "filename": "/.git/hooks/commit-msg.sample"}, {"audio": 0, "start": 89800, "crunched": 0, "end": 89989, "filename": "/.git/hooks/post-update.sample"}, {"audio": 0, "start": 89989, "crunched": 0, "end": 90413, "filename": "/.git/hooks/pre-applypatch.sample"}, {"audio": 0, "start": 90413, "crunched": 0, "end": 92055, "filename": "/.git/hooks/pre-commit.sample"}, {"audio": 0, "start": 92055, "crunched": 0, "end": 95666, "filename": "/.git/hooks/update.sample"}, {"audio": 0, "start": 95666, "crunched": 0, "end": 96905, "filename": "/.git/hooks/prepare-commit-msg.sample"}, {"audio": 0, "start": 96905, "crunched": 0, "end": 98253, "filename": "/.git/hooks/pre-push.sample"}, {"audio": 0, "start": 98253, "crunched": 0, "end": 98731, "filename": "/.git/hooks/applypatch-msg.sample"}, {"audio": 0, "start": 98731, "crunched": 0, "end": 103629, "filename": "/.git/hooks/pre-rebase.sample"}, {"audio": 0, "start": 103629, "crunched": 0, "end": 275010, "filename": "/.git/objects/pack/pack-eebfa5d82b2caaab562ba917883d88efb7779ed3.pack"}, {"audio": 0, "start": 275010, "crunched": 0, "end": 279582, "filename": "/.git/objects/pack/pack-eebfa5d82b2caaab562ba917883d88efb7779ed3.idx"}, {"audio": 0, "start": 279582, "crunched": 0, "end": 279623, "filename": "/.git/refs/heads/master"}, {"audio": 0, "start": 279623, "crunched": 0, "end": 279655, "filename": "/.git/refs/remotes/origin/HEAD"}, {"audio": 0, "start": 279655, "crunched": 0, "end": 286858, "filename": "/assets/solved.png"}, {"audio": 0, "start": 286858, "crunched": 0, "end": 287006, "filename": "/assets/empty.png"}, {"audio": 0, "start": 287006, "crunched": 0, "end": 287160, "filename": "/assets/line.png"}, {"audio": 0, "start": 287160, "crunched": 0, "end": 287319, "filename": "/assets/t.png"}, {"audio": 0, "start": 287319, "crunched": 0, "end": 449739, "filename": "/assets/Roboto-Light.ttf"}, {"audio": 0, "start": 449739, "crunched": 0, "end": 450958, "filename": "/assets/font.png"}, {"audio": 0, "start": 450958, "crunched": 0, "end": 509422, "filename": "/assets/Inconsolata.otf"}, {"audio": 0, "start": 509422, "crunched": 0, "end": 509584, "filename": "/assets/end.png"}, {"audio": 0, "start": 509584, "crunched": 0, "end": 509752, "filename": "/assets/cross.png"}, {"audio": 0, "start": 509752, "crunched": 0, "end": 509913, "filename": "/assets/curve.png"}], "remote_package_size": 509913, "package_uuid": "aedbf0d1-0bb9-4831-9976-69b353f8ab57"});

})();
